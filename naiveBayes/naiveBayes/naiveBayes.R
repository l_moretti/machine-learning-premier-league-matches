

# ATTENZIONE:  il modello bayesiano non � tropppo adatto se le probabilit� a priori sono molto diverse tra di loro
setwd("C:/Users/Luca/Documents/Universit�/Magistrale/DATATEC/progetto/progetto-dt-ml/naiveBayes")

season = read.csv("../season_merge.csv", stringsAsFactors= F,strip.white = TRUE,header = TRUE, sep = ",") 

str(season)


#variabile fattore -> ci dice se � spam o meno
season$FTR = factor(season$FTR)

season= season[ , names(season) %in% c("FTR","HomeTeam","AwayTeam","HTS","ATS","HST","AST","HTHG","HTAG","RC")] ## works as expected


library(tm)





#addestro di nuovo il naive bayes

 library(e1071)

 
 #xtab= table(pred, testset$FTR)
#library(caret) 
#confusionMatrix(xtab)

#Create 10 equally size folds
folds <- cut(seq(1,nrow(season)),breaks=10,labels=FALSE)
accuracies = c()
#Perform 10 fold cross validation
for(i in 1:10){
  #Segement your data by fold using the which() function 
  testIndexes <- which(folds==i,arr.ind=TRUE)
  testData <- season[testIndexes, ]
  trainData <- season[-testIndexes, ]
  #Use the test and train data partitions however you desire...
  
  fit  <- naiveBayes(FTR ~ ., trainData)
  
  predictions = predict(fit, testData)
  
  correct_count = sum(predictions == testData$FTR)
  accuracies = append(correct_count / nrow(testData), accuracies)
  
  
}


accuracies

s=sum(accuracies)
m=max(accuracies)

cat("media accuratezza fold: ", s/10)
cat("massima accuratezza fold: ", m)
 
 
